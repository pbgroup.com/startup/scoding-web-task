# Scoding web task
===================

Install per instructions:

```shell
cp .env.example .env
composer install
npm install && npm run dev
```

## Database

Provde credentials in *.env*.

```shell
source .env
php artisan migrate
```

## Seeds

```shell
php artisan db:seed --class AdminSeeder
php artisan db:seed --class AdminUserSeeder
php artisan db:seed --class UserSeeder
php artisan db:seed --class TaskSeeder
```

## Serve

```shell
source .env
php artisan serve
```

## Use

Open it up with a web browser in a usual configured port (**8000**) - e. g. *Homestead* machine.

Register with your own user - default *Laravel* provided role.

## Admin login credentials:

* Username: *scodingweb@gmail.com*
* Password: *brilius_task*

## Testing

```shell
php artisan config:clear
php artisan test
```

## FAQ

Run

```shell
composer dump-autoload
```

in case you experience *CLI* - **artisan** - commands problems.

Email [mailto:pbrilius@gmail.com](pbrilius@gmail.com) in case you experience problems or all *+370 622 98571*.
