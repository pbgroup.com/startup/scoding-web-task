<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Role extends Model
{
    use SoftDeletes;

    const ADMIN = 'admin';

    /**
     * Users many to many case
     *
     */
    public function users()
    {
        return $this->belongsToMany(User::class);
    }
}
