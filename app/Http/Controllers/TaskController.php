<?php

namespace App\Http\Controllers;

use App\Task;
use App\User;
use Facade\FlareClient\Http\Exceptions\NotFound;
use Illuminate\Http\Request;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tasks = Task::all();

        return view('tasks.list');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('tasks.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = User::where('name', $request->input(3)['value'])->get();

        if (sizeof($user) === 0) {
            throw new NotFound('User not found', 404);
        }

        $task = new Task([
            'label' => $request->input(0)['value'],
            'dueDate' => $request->input(1)['value'],
            'status' => $request->input(2)['value'],
            'user_id' => $user[0]->id,
        ]);

        $task->save();

        return response('', 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function show(Task $task)
    {
        return view('tasks.show', [
            'task' => $task,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function edit(Task $task)
    {
        return view('tasks.edit', [
            'task' => $task,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Task $task)
    {
        $userUpdate = User::where('name', $request->input(3)['value'])->get();

        $task->update([
            'label' => $request->input(0)['value'],
            'dueDate' => $request->input(1)['value'],
            'status' => $request->input(2)['value'],
            'user_id' => $userUpdate[0]->id,
        ]);

        return response('', 202);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function destroy(Task $task)
    {
        //
    }
}
