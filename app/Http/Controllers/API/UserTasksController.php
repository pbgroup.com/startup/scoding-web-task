<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Task;
use App\User;
use Illuminate\Http\Request;

class UserTasksController extends Controller
{
    /**
     * Display a listing of the resource.
     * 
     * @param string $id
     *
     * @return \Illuminate\Http\Response
     */
    public function index(string $id)
    {
        $tasks = User::find($id)->tasks()->get();

        return response()->json(
            $tasks
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $idUser
     * @param  int  $idTask
     * @return \Illuminate\Http\Response
     */
    public function show($idUser, $idTask)
    {
        $tasks = User::find($idUser)->tasks()->get();

        $task = $tasks->find($idTask);

        return response()->json(
            $task
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $idUser
     * @param  int  $idTask
     * @return \Illuminate\Http\Response
     */
    public function destroy($idUser, $idTask)
    {
        Task::destroy($idTask);

        return response('', 204);
    }
}
