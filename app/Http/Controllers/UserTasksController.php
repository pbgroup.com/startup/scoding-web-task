<?php

namespace App\Http\Controllers;

use App\Task;
use App\User;
use Illuminate\Http\Request;

class UserTasksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param string $id
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        return view('user-tasks.list', [
            'userId' => $id,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $idUser
     * @param int $idTask
     *
     * @return \Illuminate\Http\Response
     */
    public function show($idUser, $idTask)
    {
        $task = Task::find($idTask);

        return view('user-tasks.show', [
            'task' => $task,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $userId
     * @param int $taskId
     * 
     * @return \Illuminate\Http\Response
     */
    public function edit(int $userId, int $taskId)
    {
        $task = Task::find($userId);

        return view('user-tasks.edit', [
            'task' => $task,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string  $taskId
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, string $taskId)
    {
        $task = Task::find($taskId);

        $task->update([
            'status' => $request->input(0)['value']
        ]);

        return response('', 202);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function destroy(Task $task)
    {
        //
    }
}
