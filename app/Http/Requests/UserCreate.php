<?php

namespace App\Http\Requests;

use App\User as AppUser;
use Illuminate\Foundation\Http\FormRequest;

class UserCreate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $user = AppUser::find($this->route('user'));

        return $user && $this->user()->can('create', $user);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|max:255',
            'hash' => 'required',
            'hash_repeat' => 'required',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'email.required' => 'An email is required',
            'hash.required'  => 'A hash is required',
            'hash_repeat.required'  => 'A hash repeat is required',
        ];
    }
}
