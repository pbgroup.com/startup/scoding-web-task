<?php

namespace App\Http\Middleware;

use App\Role;
use Closure;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Support\Facades\Auth;

class LetUserThrough
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Auth::user();
        preg_match('/(\w+\/)?\w+\/(?<userId>\d+)\/\w+/', $request->path(), $matches);

        if ($user->id !== (int) $matches['userId'] && !$user->hasRole(Role::ADMIN)) {
            throw new AuthorizationException('User doesn\'t match records');
        }

        return $next($request);
    }
}
