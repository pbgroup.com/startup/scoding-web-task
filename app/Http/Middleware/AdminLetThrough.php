<?php

namespace App\Http\Middleware;

use App\Role;
use App\User;
use Closure;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Support\Facades\Auth;

class AdminLetThrough
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        /**
         * User auth instance
         *
         * @var User $user
         */
        $user = Auth::user();

        if (!$user->hasRole(Role::ADMIN)) {
            throw new AuthorizationException('Not ' . Role::ADMIN . ' role');
        }

        return $next($request);
    }
}
