<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Task extends Model
{
    use SoftDeletes;

    protected $fillable = ['label', 'dueDate', 'user_id', 'status'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
