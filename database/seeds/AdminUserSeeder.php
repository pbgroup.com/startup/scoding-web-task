<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class AdminUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $id = DB::table('users')->insertGetId([
            'name' => 'Scoding admin',
            'email' => 'scodingweb@gmail.com',
            'password' => Hash::make('brilius_task'),
        ]);

        $role = DB::table('roles')->where([
            ['label', '=','admin'],
        ])->get();

        DB::table('role_user')->insert([
            'user_id' => $id,
            'role_id' => $role[0]->id,
        ]);
    }
}
