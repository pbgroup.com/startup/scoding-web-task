<?php

use App\Task;
use App\User;
use Illuminate\Database\Seeder;

class TaskSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(User::class, 48)->create()->each(function ($user) {
            $user->tasks()->createMany(factory(Task::class, 14)->make()->toArray());
        });
    }
}
