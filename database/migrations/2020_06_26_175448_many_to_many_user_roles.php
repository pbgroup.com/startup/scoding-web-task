<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ManyToManyUserRoles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('role_user', function (Blueprint $table) {
            $table->foreignId('user_id');
            $table->foreignId('role_id');
        });

        DB::statement('ALTER TABLE `role_user` ADD FOREIGN KEY(`user_id`) REFERENCES `users`(`id`)');
        DB::statement('ALTER TABLE `role_user` ADD FOREIGN KEY(`role_id`) REFERENCES `roles`(`id`)');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('role_user', function (Blueprint $table) {
            $table->dropForeign('role_user_ibfk_1');
            $table->dropForeign('role_user_ibfk_1');
            $table->dropColumn('user_id');
            $table->dropColumn('role_id');
        });
    }
}
