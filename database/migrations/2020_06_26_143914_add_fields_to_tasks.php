<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class AddFieldsToTasks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tasks', function (Blueprint $table) {
            $table->string('label');
            $table->date('dueDate');
            $table->foreignId('user_id');
        });

        DB::statement('ALTER TABLE `tasks` ADD FOREIGN KEY (`user_id`) REFERENCES `users`(`id`)');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tasks', function (Blueprint $table) {
            $table->dropColumn('label');
            $table->dropColumn('dueDate');
            $table->dropForeign('tasks_ibfk_1');
            $table->dropColumn('user_id');
        });
    }
}
