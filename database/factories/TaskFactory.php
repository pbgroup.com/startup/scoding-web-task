<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\Task;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(Task::class, function (Faker $faker) {
    return [
        'label' => Str::random(12),
        'dueDate' => $faker->date('Y-m-d', '+2M'),
        'status' => 'to-do',
    ];
});
