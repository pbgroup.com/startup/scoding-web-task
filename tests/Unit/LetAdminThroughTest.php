<?php

namespace Tests\Unit;

use App\Http\Middleware\AdminLetThrough;
use Illuminate\Auth\Access\AuthorizationException;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class LetAdminThroughTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testExample()
    {
        $letAdmin = $this
            ->getMockBuilder(AdminLetThrough::class)
            ->disableProxyingToOriginalMethods()
            ->getMock();

        $letAdmin
            ->expects($this->atLeastOnce())
            ->method('handle')
            ->willReturn($this->prophesize(ResponseInterface::class)->reveal());

        $this->assertInstanceOf(ResponseInterface::class, $letAdmin->handle($this->prophesize(RequestInterface::class)->reveal(), function() {}));

        $letAdmin
            ->expects($this->once())
            ->method('handle')
            ->willThrowException($this->prophesize(AuthorizationException::class)->reveal());

        try {
            $letAdmin->handle($this->prophesize(RequestInterface::class)->reveal(), function () {
            });
        } catch (AuthorizationException $e) {

        }
    }
}
