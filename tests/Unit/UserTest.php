<?php

namespace Tests\Unit;

use App\Role;
use App\User;
use PHPUnit\Framework\TestCase;

class UserTest extends TestCase
{
    /**
     * User test case - model
     *
     * @return void
     */
    public function testModelPermutation()
    {
        $user = $this
            ->getMockBuilder(User::class)
            ->disableOriginalConstructor()
            ->disallowMockingUnknownTypes()
            ->disableProxyingToOriginalMethods()
            ->getMock();

        $this->assertObjectHasAttribute('fillable', $user);
        $this->assertObjectHasAttribute('hidden', $user);
        $this->assertObjectHasAttribute('casts', $user);
    }

    /**
     * ORM Eloquent relationship case
     *
     * @return void
     */
    public function testModelRelationship()
    {
        $user = $this
            ->getMockBuilder(User::class)
            ->disableOriginalConstructor()
            ->disallowMockingUnknownTypes()
            ->disableProxyingToOriginalMethods()
            ->setMethods(['roles', 'hasRole'])
            ->getMock();

        $user
            ->expects($this->once())
            ->method('roles')
            ->willReturn([]);

        $user
            ->expects($this->once())
            ->method('hasRole')
            ->willReturn(true);

        $this->assertIsArray($user->roles());
        $this->assertTrue($user->hasRole(Role::ADMIN));
    }
}
