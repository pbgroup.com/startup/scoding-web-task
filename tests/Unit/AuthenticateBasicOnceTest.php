<?php

namespace Tests\Unit;

use App\Http\Middleware\AuthenticateOnceWithBasicAuth;
use Illuminate\Auth\Access\AuthorizationException;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class AuthenticateBasicOnceTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testExample()
    {
        $basicAuth = $this
            ->getMockBuilder(AuthenticateOnceWithBasicAuth::class)
            ->disableProxyingToOriginalMethods()
            ->getMock();

        $basicAuth
            ->expects($this->atLeastOnce())
            ->method('handle')
            ->willReturn($this->prophesize(ResponseInterface::class)->reveal());

        $this->assertInstanceOf(ResponseInterface::class, $basicAuth->handle($this->prophesize(RequestInterface::class)->reveal(), function () {
        }));

        $basicAuth = $this
            ->getMockBuilder(AuthenticateOnceWithBasicAuth::class)
            ->disableProxyingToOriginalMethods()
            ->getMock();

        $basicAuth
            ->expects($this->once())
            ->method('handle')
            ->willReturn(null);

        $this->assertNull($basicAuth->handle($this->prophesize(RequestInterface::class)->reveal(), function () {
        }));
    }
}
