<?php

namespace Tests\Unit;

use App\Task;
use App\User;
use Illuminate\Database\Eloquent\Model;
use PHPUnit\Framework\TestCase;

class TaskTest extends TestCase
{
    /**
     * Task case unit - model
     *
     * @return void
     */
    public function testModelPermutation()
    {
        $task = $this 
            ->getMockBuilder(Task::class)
            ->disableOriginalConstructor()
            ->disableProxyingToOriginalMethods()
            ->disallowMockingUnknownTypes()
            ->getMock();

        $task
            ->expects($this->once())
            ->method('user')
            ->willReturn($this->prophesize(User::class)->reveal());

        $this->assertInstanceOf(Model::class, $task->user());
    }
}
