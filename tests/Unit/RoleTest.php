<?php

namespace Tests\Unit;

use App\Role;
use PHPUnit\Framework\TestCase;

class RoleTest extends TestCase
{
    /**
     * Role model test case
     *
     * @return void
     */
    public function testEtestModelPermutation()
    {
        $role = $this
            ->getMockBuilder(Role::class)
            ->disableOriginalConstructor()
            ->disableProxyingToOriginalMethods()
            ->disallowMockingUnknownTypes()
            ->getMock();

        $role
            ->expects($this->once())
            ->method('users')
            ->willReturn([]);

        $this->assertObjectHasAttribute('forceDeleting', $role);
        $this->assertIsArray($role->users());
    }
}
