<?php

namespace Tests\Unit;

use App\Http\Middleware\LetUserThrough;
use Illuminate\Auth\Access\AuthorizationException;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class LetUserThroughTest extends TestCase
{
    /**
     * Unit case for user space middleware
     *
     * @return void
     */
    public function testExample()
    {
        $letUser = $this
            ->getMockBuilder(LetUserThrough::class)
            ->disableProxyingToOriginalMethods()
            ->getMock();

        $letUser
            ->expects($this->atLeastOnce())
            ->method('handle')
            ->willReturn($this->prophesize(ResponseInterface::class)->reveal());

        $this->assertInstanceOf(ResponseInterface::class, $letUser->handle($this->prophesize(RequestInterface::class)->reveal(), function() {}));

        $letUser
            ->expects($this->once())
            ->method('handle')
            ->willThrowException($this->prophesize(AuthorizationException::class)->reveal());

        try {
            $letUser->handle($this->prophesize(RequestInterface::class)->reveal(), function () {
            });
        } catch (AuthorizationException $e) {

        }
    }
}
