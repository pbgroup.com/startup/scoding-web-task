@extends('layouts.app')

@section('content')
    <p>
        <h1>
            {{ __('Task show - single') }}
        </h1>
    </p>
    <datalist data-task-id="{{ $task->id }}" data-user-id="{{ $task->user_id }}">
    </datalist>
    <div class="container-fluid">
        <user-task-show></user-task-show>
    </div>
@endsection