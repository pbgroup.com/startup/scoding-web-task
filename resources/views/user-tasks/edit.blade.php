@extends('layouts.app')

@section('content')
<p>
    <h1>
        Your task update
    </h1>
</p>
<datalist data-task-id="{{ $task->id }}" data-user-id="{{ $task->user_id }}"></datalist>
<div class="container-fluid">
    <div class="alert alert-primary d-none" role="alert">Data updated successfully</div>
    <div class="alert alert-danger d-none" role="alert">Data updating failed</div>
    <user-task-edit></user-task-edit>
</div>
@endsection