@extends('layouts.app')

@section('content')
<p>
    <h1>
        Your tasks
    </h1>
</p>
<div class="container-fluid">
    <data-list data-user-id={{ $userId }}></data-list>
    <user-tasks></user-tasks>
</div>
@endsection