@extends('layouts.app')

@section('content')
    <p>
        <h1>
            {{ __('Task show - single') }}
        </h1>
    </p>
    <datalist data-task-id="{{ $task->id }}">
    </datalist>
    <div class="container-fluid">
        <task-show></task-show>
    </div>
@endsection