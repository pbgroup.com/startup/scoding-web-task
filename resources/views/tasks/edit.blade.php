@extends('layouts.app')

@section('content')
<p>
    <h1>
        Task update
    </h1>
</p>
<datalist data-task-id="{{ $task->id }}"></datalist>
<div class="container-fluid">
    <div class="alert alert-primary d-none" role="alert">Data updated successfully</div>
    <div class="alert alert-danger d-none" role="alert">Data updating failed</div>
    <task-edit></task-edit>
</div>
@endsection