@extends('layouts.app')

@section('content')
<p>
    <h1>
        Tasks list
    </h1>
</p>
<div class="container container-lg">
    <div class="list-group">
        <a href="{{ url('tasks/create') }}" class="list-list-group-item list-group-item-primary">Add task</a>
    </div>
</div>
<div class="container-fluid" id="tasks-list">
    <tasks-list></tasks-list>
</div>
@endsection