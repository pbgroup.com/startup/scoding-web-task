@extends('layouts.app')

@section('content')
<p>
    <h1>
        Add task
    </h1>
</p>
<div class="container-fluid">
    <div class="alert alert-primary d-none" role="alert">Data added successfully</div>
    <div class="alert alert-danger d-none" role="alert">Data adding failed</div>
    <task-create></task-create>
</div>
@endsection