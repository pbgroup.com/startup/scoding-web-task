@extends('layouts.app')

@section('content')
<p>
    <h1>
        User update
    </h1>
</p>
<datalist data-user-id="{{ $user->id }}"></datalist>
<div class="container-fluid">
    <div class="alert alert-primary d-none" role="alert">Data updated successfully</div>
    <div class="alert alert-danger d-none" role="alert">Data updating failed</div>
    <user-edit></user-edit>
</div>
@endsection