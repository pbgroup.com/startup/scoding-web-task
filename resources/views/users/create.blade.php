@extends('layouts.app')

@section('content')
<p>
    <h1>
        Add user
    </h1>
</p>
<div class="container-fluid">
    <div class="alert alert-primary d-none" role="alert">Data added successfully</div>
    <div class="alert alert-danger d-none" role="alert">Data adding failed</div>
    <user-create></user-create>
</div>
@endsection