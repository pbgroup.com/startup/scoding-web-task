@extends('layouts.app')

@section('content')
<p>
    <h1>
        Users list
    </h1>
</p>
<div class="container container-lg">
    <div class="list-group">
        <a href="{{ url('users/create') }}" class="list-list-group-item list-group-item-primary">Add user</a>
    </div>
</div>
<div class="container-fluid" id="users-list">
    <users-list></users-list>
</div>
@endsection