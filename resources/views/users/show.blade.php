@extends('layouts.app')

@section('content')
    <p>
        <h1>
            User show - single
        </h1>
    </p>
    <datalist data-user-id="{{ $user->id }}">
    </datalist>
    <div class="container-fluid" id="user-show">
        <user-show></user-show>
    </div>
@endsection