/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

const { default: Axios } = require('axios');

require('./bootstrap');

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('example-component', require('./components/ExampleComponent.vue').default);

Vue.component('users-list', require('./components/users/List.vue').default);
Vue.component('user-show', require('./components/users/Show.vue').default);
Vue.component('user-edit', require('./components/users/Edit.vue').default);
Vue.component('user-create', require('./components/users/Create.vue').default);

Vue.component('tasks-list', require('./components/tasks/List.vue').default);
Vue.component('task-show', require('./components/tasks/Show.vue').default);
Vue.component('task-edit', require('./components/tasks/Edit.vue').default);
Vue.component('task-create', require('./components/tasks/Create.vue').default);

Vue.component('user-tasks', require('./components/user-tasks/List.vue').default);
Vue.component('user-task-show', require('./components/user-tasks/Show.vue').default);
Vue.component('user-task-edit', require('./components/user-tasks/Edit.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
});
